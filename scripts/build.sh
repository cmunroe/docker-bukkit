#!/bin/bash

# Update
apk update
apk --no-cache add wget git bash

# Make Folders
mkdir -p /minecraft
cd /minecraft

# Get Builder
wget -qO /minecraft/BuildTools.jar "https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar" 

echo Version: $1

# Build Minecraft
java -jar BuildTools.jar --rev $1

# Verify Spigot Build
if ls /minecraft/spigot-* 1> /dev/null 2>&1; then
    echo Found spigot.
    mv /minecraft/spigot-*.jar /builds/
else
    ls /minecraft/
    exit 1
fi 

# Verify CraftBukkit Build
if ls /minecraft/craftbukkit-* 1> /dev/null 2>&1; then
    echo Found craftbukkit.
else
    echo Missing craftbukkit.
    java -jar BuildTools.jar --compile craftbukkit --rev $1
fi

# Capture Multiple Locations. 
if ls /minecraft/craftbukkit-* 1> /dev/null 2>&1; then
    mv /minecraft/craftbukkit-*.jar /builds/
else
    mv /minecraft/CraftBukkit/target/craftbukkit-*.jar /builds/
fi
