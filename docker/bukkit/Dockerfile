# Bukkit for Docker
#     Copyright (C) 2015 Bren Briggs

#     This program is free software; you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation; either version 2 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License along
#     with this program; if not, write to the Free Software Foundation, Inc.,
#     51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

ARG JAVA_VERSION=22
FROM eclipse-temurin:${JAVA_VERSION}-jdk-alpine

RUN apk add --no-cache python3 bash git py3-pip py3-setuptools && \
    apk update && \
    apk upgrade && \  
    if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi && \
    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi && \
    mkdir -p /opt/minecraft

ARG MINECRAFT_VERSION=1.20.6
COPY builds/craftbukkit-${MINECRAFT_VERSION}.jar /opt/minecraft/craftbukkit.jar
ADD entrypoint.sh /opt/minecraft/entrypoint.sh
ADD configure.py /opt/minecraft/configure.py

RUN chmod +x /opt/minecraft/entrypoint.sh

EXPOSE 25565
WORKDIR /data
ENV InitRAM=1g
ENV MaxRAM=2g

HEALTHCHECK --interval=1m --timeout=10s \
  CMD nc -z localhost 25565 || exit 1

ENTRYPOINT ["/opt/minecraft/entrypoint.sh"]

CMD ["craftbukkit"]
